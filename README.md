# Atmos

Implementation attempts of atmosphere simulation techniques.

# Gallery

![Bottom right sun](https://imgur.com/tSfIAg3.png)

![Upper left sun](https://imgur.com/lPOLuEu.png)

By modifying the values, special effects can be achieved:

![Radioactive planet](https://imgur.com/LWYIp7i.png)

# Resources

- Nishita et al. (1993) - [Display of the Earth Taking into Account Atmospheric Scattering](http://nishitalab.org/user/nis/cdrom/sig93_nis.pdf)
- Scratchapixel - [Simulating the Colors of the Sky](https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/simulating-sky)
- Lopes et al. (2014) - [Atmospheric Scattering - State of the Art](https://core.ac.uk/download/pdf/55631247.pdf)
- O'Neil/GPU Gems 2 - [Accurate Atmospheric Scattering](https://developer.nvidia.com/gpugems/gpugems2/part-ii-shading-lighting-and-shadows/chapter-16-accurate-atmospheric-scattering)
- Bruneton et al. (2008) - [Precomputed Atmospheric Scattering](http://www-ljk.imag.fr/Publications/Basilic/com.lmc.publi.PUBLI_Article@11e7cdda2f7_f64b69/article.pdf)
- Sebastien Hillaire (2020) - [A Scalable and Production Ready Sky and Atmosphere Rendering Technique](https://sebh.github.io/publications/egsr2020.pdf) ([code](https://github.com/sebh/UnrealEngineSkyAtmosphere))
